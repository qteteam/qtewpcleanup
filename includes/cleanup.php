<?php
namespace QTEWPCLEANUP\Cleanup;

if( ! defined('ABSPATH')) {
    exit;
}

//Wordpress cleanup & Hacks

//Remove wp welcome panel
remove_action('welcome_panel', __NAMESPACE__ . '\\wp_welcome_panel');

//Remove version control
function remove_version() {
  return '';
}
add_filter('the_generator', __NAMESPACE__ . '\\remove_version');

function removeHeaderStuff() {
  // Remove stuff from wp_head()
  remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); // Emoji
  remove_action( 'wp_print_styles', 'print_emoji_styles' ); // Emoji css
  remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
  remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
  remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
  remove_action( 'wp_head', 'index_rel_link' ); // index link
  remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
  remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
  remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
  remove_action( 'wp_head', 'rel_canonical' );
  remove_action( 'wp_head', 'wp_shortlink_wp_head' );
  remove_action( 'wp_head', 'rest_output_link_wp_head' );
  remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\removeHeaderStuff' );