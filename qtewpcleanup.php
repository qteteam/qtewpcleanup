<?php
/*
 * Plugin Name: QTE Wp Cleanup
 * Description: Cleans up wordpress header
 * Version: 0.1.0
 * Author: QTE Development AB
 * Author URI: https://getqte.se/
 */

 if( ! defined('ABSPATH')) {
     exit;
 }

 define('QTEWPCLEANUP_VERSION', '0.1.0');

define( 'QTEWPCLEANUP_PLUGIN_DIR', plugin_dir_url( __FILE__ ) );
define( 'QTEWPCLEANUP_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );


require_once QTEWPCLEANUP_PLUGIN_PATH . 'includes/cleanup.php';

